package us.genly.elephants.actors;

import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.ERectangle;
import us.genly.elephants.graphics.Random;

public class Mouse extends Actor {
    private final Room _room;
    private final MouseActor _actor;
    private final ERectangle _eRectangle = new ERectangle();
    private int _steps = 0;
    private int _age = 0;

    public Mouse(Room room, int x, int y) {
        _room = room;
        _actor = new MouseActor(x, y);
        _actor.left();
    }

    public void getRect(ERectangle r) {
        _actor.getRect(r);
    }

    public void paint(EGraphics g) {
        _actor.paint(g);
    }

    public void next() {
        if (++_age > 50) {
            _room.removeActor(this);
            return;
        }
        randomMove();
        _actor.next();
    }

    private void randomMove() {
        if (--_steps <= 0) {
            _steps = (int) (20*getScale() + Random.nextInt((int) (25*getScale())));
            int d = Random.nextInt(4);
            if (d < 0) d = -d;
            d = d % 4;
            switch (d) {
                case 0:
                    _actor.left();
                    break;
                case 1:
                    _actor.right();
                    break;
                case 2:
                    _actor.forward();
                    break;
                case 3:
                    _actor.backward();
                    break;
            }
        }
        _actor.getRect(_eRectangle);
        if (_eRectangle.x + _eRectangle.width >= _room.getWidth()) _actor.left();
        else if (_eRectangle.x <= 0) _actor.right();
        else if (_eRectangle.y + _eRectangle.height >= _room.getHeight()) _actor.backward();
        else if (_eRectangle.y < 0) _actor.forward();
    }

    public void setScale(float scale) {
        super.setScale(scale);
        _actor.setScale(scale);
    }

    public float getX() {
        return _actor.getX();
    }

    public float getY() {
        return _actor.getY();
    }

    public boolean isFacingLeft() {
        return _actor.isFacingLeft();
    }

    public boolean isFacingRight() {
        return _actor.isFacingRight();
    }

    public boolean isFacingForward() {
        return _actor.isFacingForward();
    }

    public boolean isFacingBackward() {
        return _actor.isFacingBackward();
    }

}