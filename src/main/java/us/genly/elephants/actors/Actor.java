package us.genly.elephants.actors;

import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.EPoint;
import us.genly.elephants.graphics.ERectangle;


public abstract class Actor {
    private float _x;
    private float _y;
    protected float _drawScaleX;
    protected float _drawScaleY = 1f;
    private final ERectangle _tmpRect1 = new ERectangle();
    private final ERectangle _tmpRect2 = new ERectangle();
    private float _scale = 1;

    public void setScale(float scale) {
        _scale = scale;
    }

    public float getScale() {
        return _scale;
    }

    protected float getX() {
        return _x;
    }

    protected void setX(float x) {
        _x = x;
    }

    protected float getY() {
        return _y;
    }

    protected void setY(float y) {
        _y = y;
    }

    public float getDrawScaleX() {
        return _drawScaleX;
    }

    public void setDrawScaleX(float _scaleX) {
        _drawScaleX = _scaleX;
    }

    public float getDrawScaleY() {
        return _drawScaleY;
    }

    public void setDrawScaleY(float _scaleY) {
        _drawScaleY = _scaleY;
    }

    protected Actor() {
        _drawScaleX = 1f;
    }

    protected abstract void next();
    protected abstract void paint(EGraphics g);

    protected abstract void getRect(ERectangle r);

    protected void getCenter(EPoint p)
    {
        getRect(_tmpRect1);
        p.x = _tmpRect1.x + _tmpRect1.width/2;
        p.y = _tmpRect1.y + _tmpRect1.height/2;
    }
    
    protected float getWidth()
    {
        getRect(_tmpRect1);
        return _tmpRect1.width;
    }

    protected float getHeight()
    {
        getRect(_tmpRect1);
        return _tmpRect1.height;
    }

    protected boolean isFacingLeft()
    {
        return false;
    }
    
    protected boolean isFacingRight()
    {
        return false;
    }

    protected boolean isFacingForward()
    {
        return true;
    }

    protected boolean isFacingBackward()
    {
        return false;
    }
    
    protected boolean isFacing(Actor a)
    {
        getRect(_tmpRect1);
        a.getRect(_tmpRect2);
        float cx = _tmpRect2.x + _tmpRect2.width/2;
        float cy = _tmpRect2.y + _tmpRect2.height/2;

        // Find a peanut the elephant is facing.
        return isFacingLeft()     && cx < _tmpRect1.x
            || isFacingRight()    && cx > _tmpRect1.x + _tmpRect1.width
            || isFacingForward()  && cy > _tmpRect1.y
            || isFacingBackward() && cy < _tmpRect1.y;
    }
    
    protected float distanceTo(Actor a)
    {
        getRect(_tmpRect1);
        a.getRect(_tmpRect2);
        
        if (_tmpRect1.intersects(_tmpRect2))
            return 0;
        
        float dx1 = Math.abs(_tmpRect1.x - (_tmpRect2.x + _tmpRect2.width));
        float dx2 = Math.abs((_tmpRect1.x + _tmpRect1.width)  - _tmpRect2.x);
        float dx = Math.min(dx1, dx2);

        float dy1 = Math.abs(_tmpRect1.y - (_tmpRect2.y + _tmpRect2.height));
        float dy2 = Math.abs((_tmpRect1.y + _tmpRect1.height)  - _tmpRect2.y);
        float dy = Math.min(dy1, dy2);
        
        return dx+dy;
    }
    

    protected boolean intersects(Actor a)
    {
        getRect(_tmpRect1);
        a.getRect(_tmpRect2);
        return _tmpRect1.intersects(_tmpRect2);
    }

    protected void delete() {
    	
    }
    
    protected boolean contains(float x, float y)
    {
        getRect(_tmpRect1);
        return _tmpRect1.contains(x, y);
    }

    public void drawCircle(EGraphics g, float cx, float cy, float r)
    {
        g.drawOval(cx * _drawScaleX - r+_x, cy * _drawScaleX - r + _y, 2*r, 2*r);
    }

    protected void fillCircle(EGraphics g, float cx, float cy, float r)
    {
        cx = (int)(cx * _drawScaleX + _x);
        cy = (int)(cy * _drawScaleY + _y);
        r = (int)(r* _drawScaleX);
        if (r < 0)
            r = -r;
        g.fillOval(cx-r, cy-r, 2*r, 2*r);
    }
    
    protected void fillOval(EGraphics g, float x1, float y1, float w, float h)
    {
        x1 = (int)(x1* _drawScaleX + _x);
        y1 = (int)(y1* _drawScaleY + _y);
        w = (int)(w* _drawScaleX);
        if (w < 0) {
            w = -w;
            x1 -= w;
        }
        h = (int)(h* _drawScaleY);
        if (h < 0) {
            h = -h;
            y1 -= h;
        }
        g.fillOval(x1, y1, w, h);
    }

    public void drawPoint(EGraphics g, float px, float py)
    {
        g.fillRect((int)(px* _drawScaleX + _x), (int)(py* _drawScaleX + _y), (int) _drawScaleX, (int) _drawScaleY);
    }

    protected void drawLine(EGraphics g, float x1, float y1, float x2, float y2)
    {
        int x1s = (int)(x1* _drawScaleX + _x);
        int y1s = (int)(y1* _drawScaleY + _y);
        int x2s = (int)(x2* _drawScaleX + _x);
        int y2s = (int)(y2* _drawScaleY + _y);
        g.drawLine(x1s, y1s, x2s, y2s);
    }
}
