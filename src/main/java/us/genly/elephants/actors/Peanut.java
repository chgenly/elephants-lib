package us.genly.elephants.actors;
import us.genly.elephants.graphics.EColors;
import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.ERectangle;

public class Peanut extends Actor {
    private static final int LOBE_LEFT_X = -1;
    private static final int LOBE_RIGHT_X = 2;
    private static final int LOBE_RADIUS = 2;
    private static final int PEANUT_Y = 0;
    private float _lobeLeftX;
    private float _lobeRightX;
    private float _lobeRadius;

    private Elephant _elephant;
    private float _distance;

    public Peanut(int x, int y) {
        setX(x);
        setY(y);
        setScale(1);
    }

    @Override
    public void setScale(float scale) {
        super.setScale(scale);
        _lobeRightX = LOBE_RIGHT_X * scale;
        _lobeLeftX = LOBE_LEFT_X * scale;
        _lobeRadius = LOBE_RADIUS * scale;
    }

    public void next() {
    }

    public void getRect(ERectangle r) {
        r.x = getX() - _lobeLeftX - _lobeRadius;
        r.y = getY() - _lobeRightX + _lobeRadius;
        r.width = _lobeRadius + _lobeLeftX - _lobeRightX + _lobeRadius;
        r.height = _lobeRadius *2;
    }

    public void paint(EGraphics g) {
        g.setColor(EColors.yellow);
        fillCircle(g, _lobeLeftX, PEANUT_Y, _lobeRadius);
        fillCircle(g, _lobeRightX, PEANUT_Y, _lobeRadius);
    }

    public boolean claim(Elephant e) {
        float d;

        d = distance(e);

        if (_elephant != null) {
            _distance = distance(_elephant);
        }

        if (_elephant == null || d < _distance) {
            _elephant = e;
            return true;
        }
        return false;
    }

    public float distance(Elephant e)
    {
        float dx = Math.abs(e.getX() - getX());
        float dy = Math.abs(e.getY() - getY());
        return dx+dy;
    }


    public void releaseClaim()
    {
        _elephant = null;
    }

    public boolean isClaimedBy(Elephant e)
    {
        return _elephant == e;
    }

}