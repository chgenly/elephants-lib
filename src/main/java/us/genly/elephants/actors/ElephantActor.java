package us.genly.elephants.actors;

import us.genly.elephants.graphics.EColors;
import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.ERectangle;
import us.genly.elephants.graphics.Random;

import static us.genly.elephants.actors.Direction.FORWARD;

/**
 * An ElephantActors draws and animates an elephant walking in various directions.
 *
 * @author chgenly
 */
public class ElephantActor extends Actor {
    private static final int SPEED_NORMAL = 1;
    private static final int SPEED_FAST = 4;
    private static int WIDTH = 39;
    private static final int CENTER_TO_TOP = 40;
    private static final int CENTER_TO_BOTTOM = 35;
    /**
     * Distance from center to top
     */
    private float _centerToTop;
    /**
     * Distance from center to bottom
     */
    private float _centerToBottom;
    /**
     * Width of elephant bounding box
     */
    private float _width;

    private boolean _leftFootIsMovingUp;
    private int _footLeftDy = -5;
    private int _footRightDy = 5;

    private boolean _backForward = true, _frontForward = false;
    private int _sideFrontFootDx = -3, _sideBackFootDx = 3;

    private int _movementPerFrameDx, _movementPerFrameDy;
    private float _speed;
    private Direction dir = FORWARD;
    private ElephantFront _elephantFront = new ElephantFront();
    private ElephantBack _elephantBack = new ElephantBack();
    private ElephantSide _elephantLeft = new ElephantLeft();
    private ElephantSide _elephantRight = new ElephantRight();
    private ElephantGeometry _currentElephantGeometry;

    public ElephantActor(Room room) {
        setX(Random.nextInt((int) room.getWidth()));
        setY(Random.nextInt((int) room.getHeight()));
        setScale(1);
        setLeft();
        setSpeedFast(false);
    }

    public void setScale(float scale) {
        super.setScale(scale);
        _width = WIDTH * scale;
        _centerToTop = CENTER_TO_TOP * scale;
        _centerToBottom = CENTER_TO_BOTTOM * scale;
        _elephantFront.setScale(scale);
        _elephantBack.setScale(scale);
        _elephantLeft.setScale(scale);
        _elephantRight.setScale(scale);
    }

    public void paint(EGraphics g) {
//        ERectangle r = new ERectangle();
//        getRect(r);
//        g.setColor(EColors.white);
//        g.fillRect(r.x, r.y, r.width, r.height);

        _currentElephantGeometry.paint(g);
    }

    public void setShock(boolean on) {
        _elephantLeft.setShock(on);
        _elephantRight.setShock(on);
        _elephantFront.setShock(on);
        _elephantBack.setShock(on);
    }

    public void setSpeedFast(boolean fast) {
        if (fast) _speed = SPEED_FAST * getScale();
        else _speed = SPEED_NORMAL * getScale();
    }

    public void setLeft() {
        dir = Direction.LEFT;
        _movementPerFrameDx = -1;
        _movementPerFrameDy = 0;
        _currentElephantGeometry = _elephantLeft;
    }

    public void setRight() {
        dir = Direction.RIGHT;
        _movementPerFrameDx = 1;
        _movementPerFrameDy = 0;
        _currentElephantGeometry = _elephantRight;
    }

    public void setForward() {
        dir = FORWARD;
        _movementPerFrameDx = 0;
        _movementPerFrameDy = 1;
        _currentElephantGeometry = _elephantFront;
    }

    public void setBackward() {
        dir = Direction.BACKWARD;
        _movementPerFrameDx = 0;
        _movementPerFrameDy = -1;
        _currentElephantGeometry = _elephantBack;
    }

    public boolean isFacingLeft() {
        return dir == Direction.LEFT;
    }

    public boolean isFacingRight() {
        return dir == Direction.RIGHT;
    }

    public boolean isFacingForward() {
        return dir == FORWARD;
    }

    public boolean isFacingBackward() {
        return dir == Direction.BACKWARD;
    }

    public void getRect(ERectangle r) {
        r.x = getX() - _width / 2;
        r.y = getY() - _centerToTop;
        r.width = _width;
        r.height = _centerToTop + _centerToBottom;
    }

    public ERectangle getRect() {
        ERectangle r = new ERectangle();
        r.x = getX() - _width / 2;
        r.y = getY() - _centerToTop;
        r.width = _width;
        r.height = _centerToTop + _centerToBottom;
        return r;
    }

    public float getMouthX() {
        return _currentElephantGeometry.getMouthX();
    }

    public float getMouthY() {
        return _currentElephantGeometry.getMouthY();
    }

    public float getTailX() {
        return _currentElephantGeometry.getTailX();
    }

    public float getTailY() {
        return _currentElephantGeometry.getTailY();
    }

    public void next() {
        _currentElephantGeometry.next();
    }

    private abstract class ElephantGeometry {
        private static final int EYE_RADIUS_SHCOCKED = 5;
        private static final int EYE_RADIUS_NORMAL = 2;
        private static final int PUPIL_RADIUS_SHCOCKED = 3;
        private static final int PUPIL_RADIUS_NORMAL = 1;
        protected float _eyeRadius = 2;
        protected float _pupilRadius = 1;
        protected float _scale;

        protected void setScale(float scale) {
            _scale = scale;
            applyScale();
        }

        protected abstract void applyScale();

        protected void setShock(boolean on) {
            if (on) {
                _pupilRadius = PUPIL_RADIUS_SHCOCKED * _scale;
                _eyeRadius = EYE_RADIUS_SHCOCKED * _scale;
            } else {
                _pupilRadius = PUPIL_RADIUS_NORMAL * _scale;
                _eyeRadius = EYE_RADIUS_NORMAL * _scale;
            }
        }

        protected abstract void paint(EGraphics g);

        protected abstract float getMouthX();

        protected abstract float getMouthY();

        protected abstract float getTailX();

        protected abstract float getTailY();

        protected void next() {
            setX(getX() + getMovementPerFrameDx());
            setY(getY() + getMovementPerFrameDy());
        }

        private float getMovementPerFrameDx() {
            return _speed * _movementPerFrameDx;
        }

        private float getMovementPerFrameDy() {
            return _speed * _movementPerFrameDy;
        }
    }

    class ElephantFront extends ElephantGeometry {
        private static final int FOOT_LEFT_X = -10;
        private static final int FOOT_RIGHT_X = 10;
        private static final int FOOT_Y = 17;
        private static final int FOOT_RADIUS = 9;
        private static final int TOE_LEFT1X = -15;
        private static final int TOE_LEFT2X = -10;
        private static final int TOE_LEFT3X = -5;
        private static final int TOE_RIGHT_1X = 5;
        private static final int TOE_RIGHT_2X = 10;
        private static final int TOE_RIGHT_3X = 15;
        private static final int TOE_Y = 23;
        private static final int TOE_RADIUS = 2;
        private static final int HEAD_X = 0;
        private static final int HEAD_Y = -22;
        private static final int HEAD_RADIUS = 15;
        private static final int TRUNK_1_X = 0;
        private static final int TRUNK_1_Y = -15;
        private static final int TRUNK_2_X = 3;
        private static final int TRUNK_2_Y = -10;
        private static final int TRUNK_3_X = 0;
        private static final int TRUNK_3_Y = -5;
        private static final int TRUNK_4_X = 5;
        private static final int TRUNK_4_Y = 0;
        private static final int TRUNK_RADIUS = 5;
        private static final int EAR_LEFT_X = -17;
        private static final int EAR_RIGHT_X = 17;
        private static final int EAR_Y = -30;
        private static final int EAR_RADIUS = 13;
        private static final int BODY_RADIUS = 19;
        private static final int BODY_X = 0;
        private static final int BODY_Y = 0;
        private static final int EYE_LEFT_X = -7;
        private static final int EYE_RIGHT_X = 4;
        private static final int EYE_Y = -25;
        private static final int PUPIL_LEFT_X = -6;
        private static final int PUPIL_RIGHT_X = 5;
        private static final int PUPIL_Y = -26;

        private float _footLeftX = -10;
        private float _footRightX = 10;
        private float _footY = 17;
        private float _footRadius = 9;
        private float _toeLeft1X = -15;
        private float _toeLeft2X = -10;
        private float _toeLeft3X = -5;
        private float _toeRight1X = 5;
        private float _toeRight2X = 10;
        private float _toeRight3X = 15;
        private float _toeY = 23;
        private float _toeRadius = 2;
        private float _headX = 0;
        private float _headY = -22;
        private float _headRadius = 15;
        private float _trunk1X = 0;
        private float _trunk1Y = -15;
        private float _trunk2X = 3;
        private float _trunk2Y = -10;
        private float _trunk3X = 0;
        private float _trunk3Y = -5;
        private float _trunk4X = 5;
        private float _trunk4Y = 0;
        private float _trunkRadius = 5;
        private float _earLeftX = -17;
        private float _earRigthX = 17;
        private float _earY = -30;
        private float _earRadius = 13;
        private float _bodyRadius = 19;
        private float _bodyX = 0;
        private float _bodyY = 0;
        private float _eyeLeftX = -7;
        private float _eyeRightX = 4;
        private float _eyeY = -25;
        private float _pupilLeftX = -6;
        private float _pupilRightX = 5;
        private float _pupilY = -26;

        protected ElephantFront() {
            setScale(1);
        }

        @Override
        protected void applyScale() {
            _footLeftX = FOOT_LEFT_X * _scale;
            _footRightX = FOOT_RIGHT_X * _scale;
            _footY = FOOT_Y * _scale;
            _footRadius = FOOT_RADIUS * _scale;
            _toeLeft1X = TOE_LEFT1X * _scale;
            _toeLeft2X = TOE_LEFT2X * _scale;
            _toeLeft3X = TOE_LEFT3X * _scale;
            _toeRight1X = TOE_RIGHT_1X * _scale;
            _toeRight2X = TOE_RIGHT_2X * _scale;
            _toeRight3X = TOE_RIGHT_3X * _scale;
            _toeY = TOE_Y * _scale;
            _toeRadius = TOE_RADIUS * _scale;
            _headX = HEAD_X * _scale;
            _headY = HEAD_Y * _scale;
            _headRadius = HEAD_RADIUS * _scale;
            _trunk1X = TRUNK_1_X * _scale;
            _trunk1Y = TRUNK_1_Y * _scale;
            _trunk2X = TRUNK_2_X * _scale;
            _trunk2Y = TRUNK_2_Y * _scale;
            _trunk3X = TRUNK_3_X * _scale;
            _trunk3Y = TRUNK_3_Y * _scale;
            _trunk4X = TRUNK_4_X * _scale;
            _trunk4Y = TRUNK_4_Y * _scale;
            _trunkRadius = TRUNK_RADIUS * _scale;
            _earLeftX = EAR_LEFT_X * _scale;
            _earRigthX = EAR_RIGHT_X * _scale;
            _earY = EAR_Y * _scale;
            _earRadius = EAR_RADIUS * _scale;
            _bodyRadius = BODY_RADIUS * _scale;
            _bodyX = BODY_X * _scale;
            _bodyY = BODY_Y * _scale;
            _eyeLeftX = EYE_LEFT_X * _scale;
            _eyeRightX = EYE_RIGHT_X * _scale;
            _eyeY = EYE_Y * _scale;
            _pupilLeftX = PUPIL_LEFT_X * _scale;
            _pupilRightX = PUPIL_RIGHT_X * _scale;
            _pupilY = PUPIL_Y * _scale;
        }

        @Override
        public void paint(EGraphics g) {
            // elephants body
            g.setColor(EColors.getBlue());
            fillCircle(g, _bodyX, _bodyY, _bodyRadius);

            // elephants legs
            fillCircle(g, _footLeftX, _footLeftDy + _footY, _footRadius);
            fillCircle(g, _footRightX, _footRightDy + _footY, _footRadius);

            // elephants toes
            g.setColor(EColors.getGray());
            fillCircle(g, _toeRight1X, _footRightDy + _toeY, _toeRadius);
            fillCircle(g, _toeRight2X, _footRightDy + _toeY, _toeRadius);
            fillCircle(g, _toeRight3X, _footRightDy + _toeY, _toeRadius);

            fillCircle(g, _toeLeft1X, _footLeftDy + _toeY, _toeRadius);
            fillCircle(g, _toeLeft2X, _footLeftDy + _toeY, _toeRadius);
            fillCircle(g, _toeLeft3X, _footLeftDy + _toeY, _toeRadius);


            // elephants ears
            g.setColor(EColors.blue);
            fillCircle(g, _earLeftX, _earY, _earRadius);
            fillCircle(g, _earRigthX, _earY, _earRadius);
            g.setColor(EColors.black);
            drawCircle(g, _earLeftX, _earY, _earRadius);
            drawCircle(g, _earRigthX, _earY, _earRadius);

            // elephants head
            g.setColor(EColors.blue);
            fillCircle(g, _headX, _headY, _headRadius);

            // elephants trunk
            g.setColor(EColors.gray);
            fillCircle(g, _trunk1X, _trunk1Y, _trunkRadius);
            fillCircle(g, _trunk2X, _trunk2Y, _trunkRadius);
            fillCircle(g, _trunk3X, _trunk3Y, _trunkRadius);
            fillCircle(g, _trunk4X, _trunk4Y, _trunkRadius);

            //elephant eyes
            g.setColor(EColors.white);
            fillCircle(g, _eyeLeftX, _eyeY, _eyeRadius);
            fillCircle(g, _eyeRightX, _eyeY, _eyeRadius);
            g.setColor(EColors.black);
            fillCircle(g, _pupilRightX, _pupilY, _pupilRadius);
            fillCircle(g, _pupilLeftX, _pupilY, _pupilRadius);
        }

        @Override
        protected float getMouthX() {
            return getX();
        }

        @Override
        protected float getMouthY() {
            return getY() + _centerToBottom;
        }

        @Override
        protected float getTailX() {
            return getX();
        }

        @Override
        protected float getTailY() {
            return getY() - _centerToTop;
        }

        @Override
        public void next() {
            // Move the elephants legs
            if (_leftFootIsMovingUp) {
                _footLeftDy += 1 * _scale;
                _footRightDy -= 1 * _scale;

                if (_footLeftDy > 5 * _scale) _leftFootIsMovingUp = false;
            } else {
                _footLeftDy -= 1 * _scale;
                _footRightDy += 1 * _scale;
                if (_footLeftDy < -5 * _scale) _leftFootIsMovingUp = true;
            }
            super.next();
        }
    }

    class ElephantBack extends ElephantGeometry {
        protected static final int BODY_X = 0;
        protected static final int BODY_Y = 0;
        protected static final int BODY_RADIUS = 20;
        protected static final int LEG_LEFT_X = -10;
        protected static final int LEG_RIGHT_X = 10;
        protected static final int LEG_Y = 17;
        protected static final int LEG_RADIUS = 9;
        protected static final int HEAD_X = 0;
        protected static final int HEAD_Y = -22;
        protected static final int HEAD_RADIUS = 15;
        protected static final int EAR_LEFT_X = -17;
        protected static final int EAR_RIGHT_X = 17;
        protected static final int EAR_Y = -30;
        protected static final int EAR_RADIUS = 13;
        protected static final int TAIL_1_X0 = 0;
        protected static final int TAIL_1_Y0 = 0;
        protected static final int TAIL_1_X1 = 0;
        protected static final int TAIL_1_Y1 = 10;
        protected static final int TAIL_2_X0 = 1;
        protected static final int TAIL_2_Y0 = 0;
        protected static final int TAIL_2_X1 = 1;
        protected static final int TAIL_2_Y1 = 10;
        protected static final int TAIL_X = 1;
        protected static final int TAIL_Y = 10;
        protected static final int TAIL_RADIUS = 2;

        float _bodyX;
        float _bodyY;
        float _bodyRadius;
        float _legLeftX;
        float _legRightX;
        float _legY;
        float _legRadius;
        float _headX;
        float _headY;
        float _headRadius;
        float _earLeftX;
        float _earRightX;
        float _earY;
        float _earRadius;
        float _tail1X0;
        float _tail1Y0;
        float _tail1X1;
        float _tail1Y1;
        float _tail2X0;
        float _tail2Y0;
        float _tail2X1;
        float _tail2Y1;
        float _tailX;
        float _tailY;
        float _tailRadius;

        protected ElephantBack() {
            setScale(1);
        }

        @Override
        protected void applyScale() {
            _bodyX = BODY_X * _scale;
            _bodyY = BODY_Y * _scale;
            _bodyRadius = BODY_RADIUS * _scale;
            _legLeftX = LEG_LEFT_X * _scale;
            _legRightX = LEG_RIGHT_X * _scale;
            _legY = LEG_Y * _scale;
            _legRadius = LEG_RADIUS * _scale;
            _headX = HEAD_X * _scale;
            _headY = HEAD_Y * _scale;
            _headRadius = HEAD_RADIUS * _scale;
            _earLeftX = EAR_LEFT_X * _scale;
            _earRightX = EAR_RIGHT_X * _scale;
            _earY = EAR_Y * _scale;
            _earRadius = EAR_RADIUS * _scale;
            _tail1X0 = TAIL_1_X0 * _scale;
            _tail1Y0 = TAIL_1_Y0 * _scale;
            _tail1X1 = TAIL_1_X1 * _scale;
            _tail1Y1 = TAIL_1_Y1 * _scale;
            _tail2X0 = TAIL_2_X0 * _scale;
            _tail2Y0 = TAIL_2_Y0 * _scale;
            _tail2X1 = TAIL_2_X1 * _scale;
            _tail2Y1 = TAIL_2_Y1 * _scale;
            _tailX = TAIL_X * _scale;
            _tailY = TAIL_Y * _scale;
            _tailRadius = TAIL_RADIUS * _scale;
        }

        public void paint(EGraphics g) {
            //elephants body
            g.setColor(EColors.blue);
            fillCircle(g, _bodyX, _bodyY, _bodyRadius);

            // elephants legs
            fillCircle(g, _legLeftX, _footRightDy + _legY, _legRadius);
            fillCircle(g, _legRightX, _footLeftDy + _legY, _legRadius);

            // elephants head
            fillCircle(g, _headX, _headY, _headRadius);

            //elephants ears
            fillCircle(g, _earLeftX, _earY, _earRadius);
            fillCircle(g, _earRightX, _earY, _earRadius);

            //elephants tail
            g.setColor(EColors.gray);
            drawLine(g, _tail1X0, _tail1Y0, _tail1X1, _tail1Y1);
            drawLine(g, _tail2X0, _tail2Y0, _tail2X1, _tail2Y1);
            fillCircle(g, _tailX, _tailY, _tailRadius);
        }

        @Override
        protected float getMouthX() {
            return getX();
        }

        @Override
        protected float getMouthY() {
            return getY() - _centerToTop;
        }

        @Override
        protected float getTailX() {
            return getX();
        }

        @Override
        protected float getTailY() {
            return getY() + _centerToBottom;
        }


        @Override
        public void next() {
            // Move the elephants legs
            if (_leftFootIsMovingUp) {
                _footLeftDy += 1 * _scale;
                _footRightDy -= 1 * _scale;

                if (_footLeftDy > 5 * _scale) _leftFootIsMovingUp = false;
            } else {
                _footLeftDy -= 1 * _scale;
                _footRightDy += 1 * _scale;
                if (_footLeftDy < -5 * _scale) _leftFootIsMovingUp = true;
            }
            super.next();
        }

    }

    abstract class ElephantSide extends ElephantGeometry {
        private static final int BODY_RADIUS = 22;
        private static final int BODY_X = 0;
        private static final int BODY_Y = 0;
        private static final int FAR_LEG_LEFT_X = -15;
        private static final int FAR_LEG_RIGHT_X = 15;
        private static final int NEAR_LEG_LEFT_X = -15;
        private static final int NEAR_LEG_RIGHT_X = 18;
        private static final int LEG_Y = 20;
        private static final int NEAR_LEG_RADIUS = 9;
        private static final int FAR_LEG_RADIUS = 7;
        private static final int NEAR_TOE_1_X = -8;
        private static final int NEAR_TOE_2_X = 26;
        private static final int NEAR_TOE_Y = 26;
        private static final int NEAR_TOE_RADIUS = 2;
        private static final int HEAD_X = 22;
        private static final int HEAD_Y = -15;
        private static final int HEAD_RADIUS = 15;
        private static final int EAR_X = 20;
        private static final int EAR_Y = -25;
        private static final int EAR_RADIUS = 12;
        private static final int EYE_X = 33;
        private static final int EYE_Y = -20;
        private static final int PUPIL_X = 34;
        private static final int PUPIL_Y = -20;
        private static final int TRUNK_1_X = 35;
        private static final int TRUNK_1_Y = -10;
        private static final int TRUNK_2_X = 37;
        private static final int TRUNK_2_Y = -5;
        private static final int TRUNK_3_X = 35;
        private static final int TRUNK_3_Y = 0;
        private static final int TRUNK_4_X = 30;
        private static final int TRUNK_4_Y = 5;
        private static final int TRUNK_RADIUS = 4;
        private static final int TAIL_1_X0 = -22;
        private static final int TAIL_1_Y0 = 0;
        private static final int TAIL_1_X1 = -22;
        private static final int TAIL_1_Y1 = 8;
        private static final int TAIL_2_X0 = -23;
        private static final int TAIL_2_Y0 = 0;
        private static final int TAIL_2_X1 = -23;
        private static final int TAIL_2_Y1 = 8;
        private static final int TAIL_X = -23;
        private static final int TAIL_Y = 8;
        private static final int TAIL_RADIUS = 2;
        private final float _sx;
        private float _bodyRadius;
        private float _bodyX;
        private float _bodyY;
        private float _farLegLeftX;
        private float _farLegRightX;
        private float _nearLegLeftX;
        private float _nearLegRightX;
        private float _legY;
        private float _farLegRadius;
        private float _nearLegRadius;
        private float _nearToe1X;
        private float _nearToe2X;
        private float _nearToeY;
        private float _nearToeRadius;
        private float _headX;
        private float _headY;
        private float _headRadius;
        private float _earX;
        private float _earY;
        private float _earRadius;
        private float _eyeX;
        private float _eyeY;
        private float _pupilX;
        private float _pupilY;
        private float _trunk1X;
        private float _trunk1Y;
        private float _trunk2X;
        private float _trunk2Y;
        private float _trunk3X;
        private float _trunk3Y;
        private float _trunk4X;
        private float _trunk4Y;
        private float _trunkRadius;
        private float _tail1X0;
        private float _tail1Y0;
        private float _tail1X1;
        private float _tail1Y1;
        private float _tail2X0;
        private float _tail2Y0;
        private float _tail2X1;
        private float _tail2Y1;
        private float _tailX;
        private float _tailY;
        private float _tailRadius;

        protected ElephantSide(float sx) {
            _sx = sx;
            setScale(1);
        }

        @Override
        protected void applyScale() {
            _bodyRadius = BODY_RADIUS * _scale;
            _bodyX = BODY_X * _scale * _sx;
            _bodyY = BODY_Y * _scale;
            _farLegLeftX = FAR_LEG_LEFT_X * _scale * _sx;
            _farLegRightX = FAR_LEG_RIGHT_X * _scale * _sx;
            _nearLegLeftX = NEAR_LEG_LEFT_X * _scale * _sx;
            _nearLegRightX = NEAR_LEG_RIGHT_X * _scale * _sx;
            _legY = LEG_Y * _scale;
            _farLegRadius = FAR_LEG_RADIUS * _scale;
            _nearLegRadius = NEAR_LEG_RADIUS * _scale;
            _nearToe1X = NEAR_TOE_1_X * _scale * _sx;
            _nearToe2X = NEAR_TOE_2_X * _scale * _sx;
            _nearToeY = NEAR_TOE_Y * _scale;
            _nearToeRadius = NEAR_TOE_RADIUS * _scale;
            _headX = HEAD_X * _scale * _sx;
            _headY = HEAD_Y * _scale;
            _headRadius = HEAD_RADIUS * _scale;
            _earX = EAR_X * _scale * _sx;
            _earY = EAR_Y * _scale;
            _earRadius = EAR_RADIUS * _scale;
            _eyeX = EYE_X * _scale * _sx;
            _eyeY = EYE_Y * _scale;
            _pupilX = PUPIL_X * _scale * _sx;
            _pupilY = PUPIL_Y * _scale;
            _trunk1X = TRUNK_1_X * _scale * _sx;
            _trunk1Y = TRUNK_1_Y * _scale;
            _trunk2X = TRUNK_2_X * _scale * _sx;
            _trunk2Y = TRUNK_2_Y * _scale;
            _trunk3X = TRUNK_3_X * _scale * _sx;
            _trunk3Y = TRUNK_3_Y * _scale;
            _trunk4X = TRUNK_4_X * _scale * _sx;
            _trunk4Y = TRUNK_4_Y * _scale;
            _trunkRadius = TRUNK_RADIUS * _scale;
            _tail1X0 = TAIL_1_X0 * _scale * _sx;
            _tail1Y0 = TAIL_1_Y0 * _scale;
            _tail1X1 = TAIL_1_X1 * _scale * _sx;
            _tail1Y1 = TAIL_1_Y1 * _scale;
            _tail2X0 = TAIL_2_X0 * _scale * _sx;
            _tail2Y0 = TAIL_2_Y0 * _scale;
            _tail2X1 = TAIL_2_X1 * _scale * _sx;
            _tail2Y1 = TAIL_2_Y1 * _scale;
            _tailX = TAIL_X * _scale * _sx;
            _tailY = TAIL_Y * _scale;
            _tailRadius = TAIL_RADIUS * _scale;
        }

        public void paint(EGraphics g) {

            //elephants far legs
            g.setColor(EColors.blue.darker());
            fillCircle(g, _farLegLeftX - _sideBackFootDx, _legY, _farLegRadius);
            fillCircle(g, _farLegRightX - _sideFrontFootDx, _legY, _farLegRadius);

            //elephants body
            g.setColor(EColors.blue);
            fillCircle(g, _bodyX, _bodyY, _bodyRadius);

            //elephants near legs
            g.setColor(EColors.blue);
            fillCircle(g, _nearLegLeftX + _sideBackFootDx, _legY, _nearLegRadius);
            fillCircle(g, _nearLegRightX + _sideFrontFootDx, _legY, _nearLegRadius);

            //elephants toes on near legs
            g.setColor(EColors.gray);
            fillCircle(g, _nearToe1X + _sideBackFootDx, _nearToeY, _nearToeRadius);
            fillCircle(g, _nearToe2X + _sideFrontFootDx, _nearToeY, _nearToeRadius);

            //elephants head
            g.setColor(EColors.blue);
            fillCircle(g, _headX, _headY, _headRadius);

            //elephants ears
            g.setColor(EColors.blue.darker());
            fillCircle(g, _earX, _earY, _earRadius);

            //elephants eye
            g.setColor(EColors.white);
            fillCircle(g, _eyeX, _eyeY, _eyeRadius);
            g.setColor(EColors.black);
            fillCircle(g, _pupilX, _pupilY, _pupilRadius);

            //elephants trunk
            g.setColor(EColors.gray);
            fillCircle(g, _trunk1X, _trunk1Y, _trunkRadius);
            fillCircle(g, _trunk2X, _trunk2Y, _trunkRadius);
            fillCircle(g, _trunk3X, _trunk3Y, _trunkRadius);
            fillCircle(g, _trunk4X, _trunk4Y, _trunkRadius);

            //elephants tail
            drawLine(g, _tail1X0, _tail1Y0, _tail1X1, _tail1Y1);
            drawLine(g, _tail2X0, _tail2Y0, _tail2X1, _tail2Y1);
            fillCircle(g, _tailX, _tailY, _tailRadius);
        }

        @Override
        protected float getMouthY() {
            return getY();
        }

        @Override
        protected float getTailY() {
            return getY();
        }

        @Override
        public void next() {
            // Move the elephants legs
            if (_frontForward) {
                _sideFrontFootDx += 1 * _scale;
                if (_sideFrontFootDx > 3 * _scale) _frontForward = false;
            } else {
                _sideFrontFootDx -= 1 * _scale;
                if (_sideFrontFootDx < -3 * _scale) _frontForward = true;
            }
            if (_backForward) {
                _sideBackFootDx += 1 * _scale;
                if (_sideBackFootDx > 3 * _scale) _backForward = false;
            } else {
                _sideBackFootDx -= 1 * _scale;
                if (_sideBackFootDx < -3 * _scale) _backForward = true;
            }

            super.next();
        }
    }

    class ElephantLeft extends ElephantSide {
        protected ElephantLeft() {
            super(-1);
        }

        @Override
        protected float getMouthX() {
            return getX() - _width / 2;
        }

        @Override
        protected float getTailX() {
            return getX() + _width / 2;
        }
    }

    class ElephantRight extends ElephantSide {
        protected ElephantRight() {
            super(1);
        }

        @Override
        protected float getMouthX() {
            return getX() + _width / 2;
        }

        @Override
        protected float getTailX() {
            return getX() - _width / 2;
        }
    }
}

