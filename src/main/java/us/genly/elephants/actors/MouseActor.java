package us.genly.elephants.actors;

import us.genly.elephants.graphics.EColors;
import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.ERectangle;

import static us.genly.elephants.actors.Direction.*;

public class MouseActor extends Actor {
    private static final int CENTER_TO_TOP = 40;
    private static final int CENTER_TO_BOTTOM = 35;
    private static final int FRONT_BODY_X = -7;
    private static final int FRONT_BODY_Y = 0;
    private static final int FRONT_BODY_WIDTH = 14;
    private static final int FRONT_BODY_HEIGHT = 18;
    private static final int FRONT_NOSE_X = 0;
    private static final int FRONT_NOSE_Y = 8;
    private static final int FRONT_NOSE_RADIUS = 2;
    private static final int FRONT_EAR_LEFT_X = -5;
    private static final int FRONT_EAR_RIGHT_X = 5;
    private static final int FRONT_EAR_Y = -1;
    private static final int FRONT_EAR_RADIUS = 4;
    private static final int FRONT_EYE_LEFT_X = -3;
    private static final int FRONT_EYE_RIGHT_X = 3;
    private static final int FRONT_EYE_Y = 4;
    private static final int FRONT_EYE_RADIUS = 2;
    private static final int FRONT_TAIL_X0 = 0;
    private static final int FRONT_TAIL_Y0 = -7;
    private static final int FRONT_TAIL_X1 = 0;
    private static final int FRONT_TAIL_Y1 = 0;
    private static final int BACK_BODY_X = 0;
    private static final int BACK_BODY_Y = 0;
    private static final int BACK_BODY_RADIUS = 7;
    private static final int BACK_EAR_LEFT_X = 4;
    private static final int BACK_EAR_Y = -6;
    private static final int BACK_EAR_RADIUS = 4;
    private static final int BACK_EAR_RIGHT_X = -3;
    private static final int BACK_TAIL_X0 = 0;
    private static final int BACK_TAIL_Y0 = 7;
    private static final int BACK_TAIL_X1 = 0;
    private static final int BACK_TAIL_Y1 = 17;
    private static final int RIGHT_BODY_X = 0;
    private static final int RIGHT_BODY_Y = 0;
    private static final int RIGHT_BODY_WIDTH = 18;
    private static final int RIGHT_BODY_HEIGHT = 14;
    private static final int RIGHT_NOSE_X = 18;
    private static final int RIGHT_NOSE_Y = 4;
    private static final int RIGHT_NOSE_RADIUS = 2;
    private static final int RIGHT_EAR_X = 10;
    private static final int RIGHT_EAR_Y = -1;
    private static final int RIGHT_EAR_RADIUS = 4;
    private static final int RIGHT_EYE_X = 12;
    private static final int RIGHT_EYE_Y = 4;
    private static final int RIGHT_EYE_RADIUS = 2;
    private static final int RIGHT_TAIL_X0 = 0;
    private static final int RIGHT_TAIL_Y0 = 8;
    private static final int RIGHT_TAIL_X1 = -13;
    private static final int RIGHT_TAIL_Y1 = 8;

    private final int _width = 39;
    private float _centerToTop;
    private float _centerToBottom;
    private float _frontBodyX;
    private float _frontBodyY;
    private float _frontBodyWidth;
    private float _frontBodyHeight;
    private float _frontNoseX;
    private float _frontNoseY;
    private float _frontNoseRadius;
    private float _frontEarLeftX;
    private float _frontEarRightX;
    private float _frontEarY;
    private float _frontEarRadius;
    private float _frontEyeLeftX;
    private float _frontEyeRightX;
    private float _frontEyeY;
    private float _frontEyeRadius;
    private float _frontTailX0;
    private float _frontTailY0;
    private float _frontTailX1;
    private float _frontTailY1;
    private float _backBodyX;
    private float _backBodyY;
    private float _backBodyRadius;
    private float _backEarLeftX;
    private float _backEarY;
    private float _backEarRadius;
    private float _backEarRightX;
    private float _backTailX0;
    private float _backTailY0;
    private float _backTailX1;
    private float _backTailY1;
    private float _rightBodyX;
    private float _rightBodyY;
    private float _rightBodyWidth;
    private float _rightBodyHeight;
    private float _rightNoseX;
    private float _rightNoseY;
    private float _rightNoseRadius;
    private float _rightEarX;
    private float _rightEarY;
    private float _rightEarRadius;
    private float _rightEyeX;
    private float _rightEyeY;
    private float _rightEyeRadius;
    private float _rightTailX0;
    private float _rightTailY0;
    private float _rightTailX1;
    private float _rightTailY1;

    private float _dx, _dy;
    private Direction _dir = FORWARD;

    public MouseActor(int x, int y) {
        setX(x);
        setY(y);
        setScale(1);
    }

    @Override
    public void setScale(float scale) {
        super.setScale(scale);
        _centerToTop = CENTER_TO_TOP * scale;
        _centerToBottom = CENTER_TO_BOTTOM * scale;
        _frontBodyX = FRONT_BODY_X * scale;
        _frontBodyY = FRONT_BODY_Y * scale;
        _frontBodyWidth = FRONT_BODY_WIDTH * scale;
        _frontBodyHeight = FRONT_BODY_HEIGHT * scale;
        _frontNoseX = FRONT_NOSE_X * scale;
        _frontNoseY = FRONT_NOSE_Y * scale;
        _frontNoseRadius = FRONT_NOSE_RADIUS * scale;
        _frontEarLeftX = FRONT_EAR_LEFT_X * scale;
        _frontEarRightX = FRONT_EAR_RIGHT_X * scale;
        _frontEarY = FRONT_EAR_Y * scale;
        _frontEarRadius = FRONT_EAR_RADIUS * scale;
        _frontEyeLeftX = FRONT_EYE_LEFT_X * scale;
        _frontEyeRightX = FRONT_EYE_RIGHT_X * scale;
        _frontEyeY = FRONT_EYE_Y * scale;
        _frontEyeRadius = FRONT_EYE_RADIUS * scale;
        _frontTailX0 = FRONT_TAIL_X0 * scale;
        _frontTailY0 = FRONT_TAIL_Y0 * scale;
        _frontTailX1 = FRONT_TAIL_X1 * scale;
        _frontTailY1 = FRONT_TAIL_Y1 * scale;
        _backBodyX = BACK_BODY_X * scale;
        _backBodyY = BACK_BODY_Y * scale;
        _backBodyRadius = BACK_BODY_RADIUS * scale;
        _backEarLeftX = BACK_EAR_LEFT_X * scale;
        _backEarY = BACK_EAR_Y * scale;
        _backEarRadius = BACK_EAR_RADIUS * scale;
        _backEarRightX = BACK_EAR_RIGHT_X * scale;
        _backTailX0 = BACK_TAIL_X0 * scale;
        _backTailY0 = BACK_TAIL_Y0 * scale;
        _backTailX1 = BACK_TAIL_X1 * scale;
        _backTailY1 = BACK_TAIL_Y1 * scale;
        _rightBodyX = RIGHT_BODY_X * scale;
        _rightBodyY = RIGHT_BODY_Y * scale;
        _rightBodyWidth = RIGHT_BODY_WIDTH * scale;
        _rightBodyHeight = RIGHT_BODY_HEIGHT * scale;
        _rightNoseX = RIGHT_NOSE_X * scale;
        _rightNoseY = RIGHT_NOSE_Y * scale;
        _rightNoseRadius = RIGHT_NOSE_RADIUS * scale;
        _rightEarX = RIGHT_EAR_X * scale;
        _rightEarY = RIGHT_EAR_Y * scale;
        _rightEarRadius = RIGHT_EAR_RADIUS * scale;
        _rightEyeX = RIGHT_EYE_X * scale;
        _rightEyeY = RIGHT_EYE_Y * scale;
        _rightEyeRadius = RIGHT_EYE_RADIUS * scale;
        _rightTailX0 = RIGHT_TAIL_X0 * scale;
        _rightTailY0 = RIGHT_TAIL_Y0 * scale;
        _rightTailX1 = RIGHT_TAIL_X1 * scale;
        _rightTailY1 = RIGHT_TAIL_Y1 * scale;
    }

    public void paint(EGraphics g) {
        switch (_dir) {
            case FORWARD:
                paintFront(g);
                break;
            case BACKWARD:
                paintBack(g);
                break;
            case LEFT:
                paintLeft(g);
                break;
            case RIGHT:
                paintRight(g);
                break;
        }

        ERectangle r = new ERectangle();
        getRect(r);
        //g.draw3DRect(r.x, r.y, r.width, r.height, true);
    }

    public void left() {
        _dir = LEFT;
        _dx = -10;
        _dy = 0;
    }

    public void right() {
        _dir = RIGHT;
        _dx = 10 * getScale();
        _dy = 0 * getScale();
    }

    public void forward() {
        _dir = FORWARD;
        _dx = 0 * getScale();
        _dy = 6 * getScale();
    }

    public void backward() {
        _dir = BACKWARD;
        _dx = 0 * getScale();
        _dy = -6 * getScale();
    }

    public boolean isFacingLeft() {
        return _dir == LEFT;
    }

    public boolean isFacingRight() {
        return _dir == RIGHT;
    }

    public boolean isFacingForward() {
        return _dir == FORWARD;
    }

    public boolean isFacingBackward() {
        return _dir == BACKWARD;
    }

    public void getRect(ERectangle r) {
        r.x = getX() - _width / 2;
        r.y = getY() - CENTER_TO_TOP;
        r.width = _width;
        r.height = CENTER_TO_TOP + CENTER_TO_BOTTOM;
    }

    public void next() {
        // Move the whole Mouse
        setX(getX() + _dx);
        setY(getY() + _dy);
    }

    private void paintFront(EGraphics g) {
        // Mouses body
        g.setColor(EColors.gray);
        fillOval(g, _frontBodyX, _frontBodyY, _frontBodyWidth, _frontBodyHeight);

        // Mouses nose
        g.setColor(EColors.red);
        fillCircle(g, _frontNoseX, _frontNoseY, _frontNoseRadius);

        // Mouses ears
        g.setColor(EColors.gray);
        fillCircle(g, _frontEarLeftX, _frontEarY, _frontEarRadius);
        fillCircle(g, _frontEarRightX, _frontEarY, _frontEarRadius);

        //Mouse eyes
        g.setColor(EColors.black);
        fillCircle(g, _frontEyeLeftX, _frontEyeY, _frontEyeRadius);
        fillCircle(g, _frontEyeRightX, _frontEyeY, _frontEyeRadius);

        //mouse tail
        g.setColor(EColors.gray);
        drawLine(g, _frontTailX0, _frontTailY0, _frontTailX1, _frontTailY1);
    }


    private void paintBack(EGraphics g) {
        // Mouses body
        g.setColor(EColors.gray);
        fillCircle(g, _backBodyX, _backBodyY, _backBodyRadius);

        // Mouses ears
        g.setColor(EColors.gray);
        fillCircle(g, _backEarLeftX, _backEarY, _backEarRadius);
        fillCircle(g, _backEarRightX, _backEarY, _backEarRadius);

        //mouse tail
        g.setColor(EColors.gray);
        drawLine(g, _backTailX0, _backTailY0, _backTailX1, _backTailY1);
    }

    private void paintLeft(EGraphics g) {
        if (_drawScaleX > 0) _drawScaleX = -_drawScaleX;
        paintSide(g);
    }

    private void paintRight(EGraphics g) {
        if (_drawScaleX < 0) _drawScaleX = -_drawScaleX;
        paintSide(g);
    }

    private void paintSide(EGraphics g) {
        // Mouses body
        g.setColor(EColors.gray);
        fillOval(g, _rightBodyX, _rightBodyY, _rightBodyWidth, _rightBodyHeight);

        // Mouses nose
        g.setColor(EColors.red);
        fillCircle(g, _rightNoseX, _rightNoseY, _rightNoseRadius);

        // Mouses ears
        g.setColor(EColors.gray);
        fillCircle(g, _rightEarX, _rightEarY, _rightEarRadius);

        //Mouse eyes
        g.setColor(EColors.black);
        fillCircle(g, _rightEyeX, _rightEyeY, _rightEyeRadius);

        //mouse tail
        g.setColor(EColors.gray);
        drawLine(g, _rightTailX0, _rightTailY0, _rightTailX1, _rightTailY1);
    }
}

