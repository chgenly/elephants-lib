package us.genly.elephants.actors;

import us.genly.elephants.graphics.EColors;
import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.EPoint;
import us.genly.elephants.graphics.ERectangle;
import us.genly.elephants.graphics.Random;

import static us.genly.elephants.actors.Elephant.State.*;

@SuppressWarnings("WeakerAccess")
public class Elephant extends Actor {
    private static boolean _showThoughts;
    private boolean _horizontalFirst;
    private final Room _room;
    private final ElephantActor _actor;
    private final ERectangle _elephantRectangle = new ERectangle();
    private final EPoint _elephantPoint = new EPoint();
    private final EPoint _otherPoint = new EPoint();
    private int _movesRemaining = 50;
    private int _d;
    private int _corner;

    // Go for this peanut!
    private Peanut _peanut;

    // Or follow this elephant.
    private Elephant _leader, _follower;

    // Or run away from this mouse;
    private Mouse _scaryMouse;

    // Depending on this _state
    enum State {
        GOING_FOR_PEANUT, FOLLOW_LEADER, RANDOM, RUN_AWAY,
        // The _leader does this.
        PARADE
    }
    private State _state;

    public Elephant(Room room) {
        _room = room;
        _actor = new ElephantActor(room);
        _actor.setLeft();
        _state = RANDOM;
    }

    @Override
    public void setScale(float scale) {
        super.setScale(scale);
        _actor.setScale(scale);
    }

    public void setX(int x) {
        _actor.setX(x);
    }

    public void setY(int y) {
        _actor.setY(y);
    }

    public void delete() {
        if (_peanut != null && _peanut.isClaimedBy(this))
            _peanut.releaseClaim();

        if (_leader != null)
            _leader.followerDeleted();
        if (_follower != null)
            _follower.leaderDeleted();
    }

    private void followerDeleted() {
        _follower = null;
    }

    private void leaderDeleted() {
        _leader = null;
        _state = RANDOM;
    }

    public static void setShowThoughts(boolean t) {
        _showThoughts = t;
    }

    public static boolean getShowThoughts() {
        return _showThoughts;
    }

    public void getRect(ERectangle r) {
        _actor.getRect(r);
    }

    public ERectangle getRect() {
        return _actor.getRect();
    }

    public void setShock(boolean on) {
        _actor.setShock(on);
    }

    private void setSpeedFast(boolean fast) {
        _actor.setSpeedFast(fast);
    }

    public void paint(EGraphics g) {
        _actor.paint(g);
        if (_showThoughts) {
            switch (_state) {
                case GOING_FOR_PEANUT:
                    g.setColor(EColors.getYellow());
                    g.drawLine(_peanut.getX(), _peanut.getY(), getMouthX(), getMouthY());
                    break;

                case FOLLOW_LEADER:
                    g.setColor(EColors.getGreen());
                    g.drawLine(_leader.getTailX(), _leader.getTailY(), getMouthX(), getMouthY());
                    break;

                case RUN_AWAY:
                    g.setColor(EColors.getRed());
                    g.drawLine(_scaryMouse.getX(), _scaryMouse.getY(), getTailX(), getTailY());
                    break;
            }
        }
    }

    public void next() {
        if (!(checkForMouse() || checkForPeanut() || checkForElephantToFollow())) {
            if (_follower != null)
                _state = PARADE;
            else
                _state = RANDOM;
        }

        setShock(_state == RUN_AWAY);
        setSpeedFast(_state == RUN_AWAY);

        switch (_state) {
            case GOING_FOR_PEANUT:
                goForPeanut();
                break;
            case FOLLOW_LEADER:
                walkTrunkTowardsTail(_leader);
                break;
            case RANDOM:
                randomMove();
                break;
            case RUN_AWAY:
                runAwayFrom(_scaryMouse);
                break;
            case PARADE:
                parade();
        }

        _actor.next();
    }

    private void parade() {
        if (_corner == -1 || reachedCorner(_corner))
            _corner = pickNextCorner(_corner);

        walkTowards(getCornerX(_corner), getCornerY(_corner));
    }

    private boolean reachedCorner(int corner) {
        float x = getCornerX(corner);
        float y = getCornerY(corner);

        return _actor.contains(x, y);
    }

    private float getCornerX(int corner) {
        float x;
        switch (corner) {
            case 0:
            case 3:
                x = _actor.getWidth();
                break;
            default:
                x = _room.getWidth() - _actor.getWidth();
                break;
        }
        return x;
    }

    private float getCornerY(int corner) {
        float y;
        switch (corner) {
            case 0:
            case 1:
                y = _actor.getHeight();
                break;
            default:
                y = _room.getHeight() - _actor.getWidth();
                break;
        }
        return y;
    }

    private int pickNextCorner(int prevCorner) {
        int corner = prevCorner + 1;
        return corner % 4;
    }

    private void goForPeanut() {
        if (!_room.contains(_peanut) || !_peanut.isClaimedBy(this)) {
            setState(RANDOM);
            _peanut = null;
            return;
        }

        if (intersects(_peanut)) {
            _room.removeActor(_peanut);
            _peanut = null;
            setState(RANDOM);
            return;
        }

        walkTowards(_peanut.getX(), _peanut.getY());
    }

    private void walkTrunkTowardsTail(Elephant e) {
        float tx, ty;

        _horizontalFirst = e.isFacingForward() || e.isFacingBackward();
        tx = e.getTailX();
        ty = e.getTailY();

        if (e.isFacingLeft())
            tx += 35;
        else if (e.isFacingRight())
            tx -= 35;
        else if (e.isFacingForward())
            ty -= 30;
        else
            ty += 30;

        walkTowards(tx, ty);
    }

    private void runAwayFrom(Actor a) {
        getCenter(_elephantPoint);
        a.getCenter(_otherPoint);

        float x = _elephantPoint.x - _otherPoint.x + _elephantPoint.x;
        float y = _elephantPoint.y - _otherPoint.y + _elephantPoint.y;
        walkTowards(x, y);
    }

    private void walkTowards(float fx, float fy) {
        int targetX = (int) fx;
        int targetY = (int) fy;
        getRect(_elephantRectangle);
        int myCenterX = (int) (_elephantRectangle.x + _elephantRectangle.width / 2);
        int myCenterY = (int) (_elephantRectangle.y + _elephantRectangle.height / 2);

        float margin = _elephantRectangle.width/4;
        if (_horizontalFirst) {
            if (targetX < myCenterX - margin) {
                _actor.setLeft();
            } else if (targetX > myCenterX + margin) {
                _actor.setRight();
            } else {
                if (targetY < myCenterY - margin) {
                    _actor.setBackward();
                } else if (targetY > myCenterY + margin) {
                    _actor.setForward();
                }
            }
        } else {
            if (targetY < myCenterY - margin)
                _actor.setBackward();
            else if (targetY > myCenterY + margin)
                _actor.setForward();
            else {
                if (targetX < myCenterX - margin) {
                    _actor.setLeft();
                } else if (targetX > myCenterX + margin)
                    _actor.setRight();
            }
        }
        System.out.flush();
    }

    private  boolean checkForPeanut() {
        _actor.getRect(_elephantRectangle);
        float peanutDistance = 500000;

        if (_peanut != null)
            peanutDistance = _peanut.distance(this);

        for (Actor o : _room.roomObjects()) {
            if (o instanceof Peanut) {
                Peanut p = (Peanut) o;

                // Find a peanut the elephant is facing.
                if (_actor.isFacingLeft()) {
                    if (p.getX() > _elephantRectangle.x)
                        continue;
                } else if (_actor.isFacingRight()) {
                    if (p.getX() < _elephantRectangle.x + _elephantRectangle.width)
                        continue;
                } else if (_actor.isFacingForward()) {
                    if (p.getY() < _elephantRectangle.y + _elephantRectangle.height)
                        continue;
                } else if (_actor.isFacingBackward()) {
                    if (p.getY() > _elephantRectangle.y)
                        continue;
                }

                float d = p.distance(this);

                if (_peanut != p && d < peanutDistance && p.claim(this)) {
                    if (_peanut != null) _peanut.releaseClaim();
                    _peanut = p;
                    setState(GOING_FOR_PEANUT);
                    _horizontalFirst = Random.nextInt(100) > 50;
                }
            }
        }

        return _state == GOING_FOR_PEANUT;
    }

    private  boolean checkForElephantToFollow() {
        Elephant l, f;

        if (_leader != null)
            return true;

        for (Actor o : _room.roomObjects()) {
            if (o instanceof Elephant) {
                Elephant e = (Elephant) o;
                if (e == this)
                    continue;
                float d = distanceToTail(e);
                if (d < 500) {

                    // Make sure we are not going to follow an elephant
                    // that is already following us.  That would make a
                    // loop.
                    for (l = e; l._leader != null; ) {
                        l = l._leader;
                        if (l == this) break;
                    }
                    if (l == this) break;

                    setState(FOLLOW_LEADER);

                    // Follow the end of the chain.
                    for (f = e; f._follower != null; ) {
                        f = f._follower;
                    }

                    setLeader(f);

                    break;
                }
            }
        }

        return _state == FOLLOW_LEADER;
    }

    private  boolean checkForMouse() {
        float closestDistance = Float.MAX_VALUE;
        float d;
        _scaryMouse = null;

        // If we've run outside of the _room.   Stop running.
        if (!_room.getRect().intersects(getRect()))
            return false;

        for (Actor o : _room.roomObjects()) {
            if (o instanceof Mouse) {
                Mouse m = (Mouse) o;
                if (!isFacing(m))
                    continue;
                d = distanceTo(m);
                if (d < closestDistance) {
                    _scaryMouse = m;
                    closestDistance = d;
                }
            }
        }

        if (_scaryMouse != null)
            setState(RUN_AWAY);

        return _scaryMouse != null;
    }

    private void setState(State state) {
        _state = state;
        if (state != FOLLOW_LEADER)
            setLeader(null);
    }


    private void setLeader(Elephant ldr) {
        if (_leader == ldr)
            return;

        if (_leader != null)
            _leader._follower = null;

        _leader = ldr;
        if (ldr != null)
            ldr._follower = this;
    }

    private float distanceToTail(Elephant e) {
        float dx = Math.abs(_actor.getX() - e.getTailX());
        float dy = Math.abs(_actor.getY() - e.getTailY());
        return dx + dy;
    }

    private void randomMove() {
        if (--_movesRemaining <= 0) {
            _movesRemaining = 20 + Random.nextInt(50);
            _d = Random.nextInt(4);
            if (_d < 0)
                _d = -_d;
            _d = _d % 4;
            switch (_d) {
                case 0:
                    _actor.setLeft();
                    break;
                case 1:
                    _actor.setRight();
                    break;
                case 2:
                    _actor.setForward();
                    break;
                case 3:
                    _actor.setBackward();
                    break;
            }
        }

        _actor.getRect(_elephantRectangle);
        if (_elephantRectangle.x + _elephantRectangle.width >= _room.getWidth())
            _actor.setLeft();
        else if (_elephantRectangle.x <= 0)
            _actor.setRight();
        else if (_elephantRectangle.y + _elephantRectangle.height >= _room.getHeight())
            _actor.setBackward();
        else if (_elephantRectangle.y < 0)
            _actor.setForward();
    }

    private int directionOf(Elephant e) {
        float dx, dy, adx, ady;

        if (e == null)
            return -1;

        dx = e._actor.getX() - _actor.getX();
        dy = e._actor.getY() - _actor.getY();
        adx = Math.abs(dx);
        ady = Math.abs(dy);

        if (adx > ady) {
            if (dx > 0)
                return 1;
            else
                return 0;
        } else {
            if (dy > 0)
                return 2;
            else
                return 3;
        }
    }

    public float getX() {
        return _actor.getX();
    }

    public float getY() {
        return _actor.getY();
    }

    private float getMouthX() {
        return _actor.getMouthX();
    }

    private float getMouthY() {
        return _actor.getMouthY();
    }

    private float getTailX() {
        return _actor.getTailX();
    }

    private float getTailY() {
        return _actor.getTailY();
    }

    public boolean isFacingLeft() {
        return _actor.isFacingLeft();
    }

    public boolean isFacingRight() {
        return _actor.isFacingRight();
    }

    public boolean isFacingForward() {
        return _actor.isFacingForward();
    }

    public boolean isFacingBackward() {
        return _actor.isFacingBackward();
    }
}