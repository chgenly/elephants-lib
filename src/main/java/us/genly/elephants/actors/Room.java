package us.genly.elephants.actors;

import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.ERectangle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Room extends Actor {
    private float _w;
    private float _h;
    private final List<Actor> _roomObjects = new ArrayList<>();
    private float _scale = 1;

    public Room(int w, int h) {
        _w = w;
        _h = h;
    }

    public void setScale(float scale) {
        _scale = scale;
        for (Actor obj : _roomObjects) {
            obj.setScale(scale);
        }
    }

    public void add(Actor o) {
        o.setScale(_scale);
        _roomObjects.add(o);
    }

    public void next() {
        for (Actor o : new ArrayList<>(_roomObjects)) {
            o.next();
        }
    }

    public void getRect(ERectangle r) {
        r.x = 0;
        r.y = 0;
        r.width = _w;
        r.height = _h;
    }

    public ERectangle getRect() {
        ERectangle r = new ERectangle();
        r.x = 0;
        r.y = 0;
        r.width = _w;
        r.height = _h;
        return r;
    }

    public void paint(EGraphics g) {
        List<Actor> sorted = new ArrayList<>(_roomObjects);
        Collections.sort(sorted, new Comparator<Actor>() {
            @Override
            public int compare(Actor o1, Actor o2) {
                return (int) (o1.getY() -o2.getY());
            }
        });
        for (Actor o : sorted) {
            o.paint(g);
        }
    }

    public float getWidth() {
        return _w;
    }

    public void setWidth(float w) {
        _w = w;
    }

    public float getHeight() {
        return _h;
    }

    public void setHeight(float h) {
        _h = h;
    }

    public List<Actor> roomObjects() {
        return _roomObjects;
    }

    public void removeActor(Actor a) {
        a.delete();
        _roomObjects.remove(a);
    }

    public boolean contains(Actor o) {
        return _roomObjects.contains(o);
    }
}