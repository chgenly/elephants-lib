package us.genly.elephants;

import us.genly.elephants.actors.Elephant;
import us.genly.elephants.actors.Mouse;
import us.genly.elephants.actors.Peanut;
import us.genly.elephants.actors.Room;
import us.genly.elephants.graphics.EGraphics;
import us.genly.elephants.graphics.Random;

/**
 * The model creates a _room and adds elephants and peanuts.
 *
 * Created by genly on 9/18/2017.
 */

public class Model {
    private final Room _room;

    public Model(int width, int height) {
        this(width, height, 5, 2);
    }

    public Model(int width, int height, int elephantCount, int peanutCount) {
            _room = new Room(width, height);
        for(int i=0; i<peanutCount; ++i) {
            createPeanut();
        }

        for(int i=0; i<elephantCount; ++i)
            _room.add(new Elephant(_room));
    }

    public synchronized void setShowThoughts(boolean show) {
        Elephant.setShowThoughts(show);
    }

    public synchronized void setScale(float scale) {
        _room.setScale(scale);
    }

    public synchronized void next() {
        _room.next();
    }

    public synchronized void paint(EGraphics g) {
        _room.paint(g);
    }

    public float getWidth() {
        return _room.getWidth();
    }

    public float getHeight() {
        return _room.getHeight();
    }

    public synchronized void createPeanut(int x, int y) {
        _room.add(new Peanut(x, y));
    }

    public synchronized void createPeanut() {
        int x = Random.nextInt((int) _room.getWidth());
        int y = Random.nextInt((int) _room.getHeight());
        createPeanut(x, y);
    }

    public synchronized void createMouse(int x, int y) {
        _room.add(new Mouse(_room, x, y));
    }

    public synchronized void createMouse() {
        int x = Random.nextInt((int) _room.getWidth());
        int y = Random.nextInt((int) _room.getHeight());
        createMouse(x, y);
    }
}
