package us.genly.elephants.graphics;

@SuppressWarnings({"WeakerAccess", "InstanceVariableNamingConvention"})
public class EColor {
    public int r, g, b;

    public EColor(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public EColor darker() {
        return new EColor(Math.max(r * 7 / 10, 0), Math.max(g * 7 / 10, 0), Math.max(b * 7 / 10, 0));
    }
}

