package us.genly.elephants.graphics;


@SuppressWarnings("WeakerAccess")
public class EColors {
	public static EColor black = new EColor(0, 0, 0);
	public static EColor white = new EColor(255, 255, 255);
	public static EColor yellow = new EColor(255, 255, 0);
	public static EColor green = new EColor(0, 255, 0);
	public static EColor red = new EColor(255, 0, 0);
	public static EColor orange = new EColor(255, 128, 0);
	public static EColor blue = new EColor(150, 150, 255);
	public static EColor gray = new EColor(128, 128, 128);
	
	public static EColor getYellow() {
		return yellow;
	}

	public static EColor getGreen() {
		return green;
	}

	public static EColor getRed() {
		return red;
	}

	public static EColor getBlue() {
		return blue;
	}

	public static EColor getGray() {
		return gray;
	}

	public static EColor getBlack() {
		return black;
	}

	public static EColor getWhite() {
		return white;
	}

	public static EColor getOrange() {
		return orange;
	}
}
