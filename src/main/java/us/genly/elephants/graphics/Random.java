package us.genly.elephants.graphics;

public class Random {
	private static final java.util.Random RAND = new java.util.Random();
	
	public static int nextInt(int range) {
		int r = RAND.nextInt();
		if (r < 0) r = -r;
		return r % range;
	}
}
