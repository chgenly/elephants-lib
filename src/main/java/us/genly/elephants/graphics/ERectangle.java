package us.genly.elephants.graphics;

@SuppressWarnings("InstanceVariableNamingConvention")
public class ERectangle {
	public float x, y, width, height;

	public boolean contains(float x2, float y2) {
		float w = width;
		float h = height;
		if (w < 0  || h < 0) {
			// At least one of the dimensions is negative...
			return false;
		}
		// Note: if either dimension is zero, tests below must return false...
		float x = this.x;
		float y = this.y;
		if (x2 < x || y2 < y) {
			return false;
		}
		w += x;
		h += y;
		// overflow || intersect
		return ((w < x || w > x2) && (h < y || h > y2));
	}

	public boolean intersects(ERectangle r) {
        float x0 = x;
        float x1 = x+width;
        float ox0 = r.x;
        float ox1 = r.x + r.width;
        if (x0 <= ox0 && ox0 <= x1 || x0 <= ox1 && ox1 <= x1) {
            float y0 = y;
            float y1 = y+height;
            float oy0 = r.y;
            float oy1 = r.y + r.height;
            if (y0 <= oy0 && oy0 <= y1 || y0 <= oy1 && oy1 <= y1) {
                return true;
            }		    
        }
        return false;
	}
}
