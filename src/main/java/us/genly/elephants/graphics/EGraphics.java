package us.genly.elephants.graphics;

public interface EGraphics {
	void drawOval(float x, float y, float w, float h);
	void fillOval(float x, float y, float w, float h);
	void fillRect(float x, float y, float w, float h);
	void drawLine(float x0, float y0, float x1, float y1);
	void setColor(EColor color);
	void setScale(float scale);
}
